# Test of twist_mux

Two nodes are sending Twist
- joy_node, that listen to the joystick
  - twist.angular.z if deadmn btn=4
- sine_node that publishes a periodic signal

They are send to twist_mux, with
- higher priority for joystick
- also a /pause_sinus Bool to stop forwarding the sine_vel
- outputs/publishes to /vel_cmd_out

It seems to work as expected.

