#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Sends info using joystick
"""

import math
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy

class JoyNode(object):
    def __init__(self):
        self.joy_vel_pub = rospy.Publisher( 'joy_vel', Twist, queue_size=10 )

    def joy_cb(self, msg):
        cmd = Twist()
        if msg.buttons[4]:
            rvz = msg.axes[1]
            cmd.angular.z = rvz
            self.joy_vel_pub.publish( cmd )
        
    def run(self):
        self.joy_sub = rospy.Subscriber('/joy', Joy, self.joy_cb)
        rospy.spin()

def main():
    rospy.init_node( 'joy_node' )
    jn = JoyNode()
    jn.run()

if __name__ == '__main__':
    main()
    
