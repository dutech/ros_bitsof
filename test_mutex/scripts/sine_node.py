#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""A sinus(t/10*pi)*sinus(t/pi) periodic signal
"""

import math
import rospy
from geometry_msgs.msg import Twist

class SineNode(object):
    def __init__(self):
        self.sine_vel_pub = rospy.Publisher( 'sine_vel', Twist, queue_size=10 )

    def run(self):
        sine = Twist()
        rate = rospy.Rate(10) # 10Hz
        while not rospy.is_shutdown():
            t = rospy.get_time()
            sine.angular.z = math.sin( t * 2 * math.pi ) * math.sin( t/math.pi )
            self.sine_vel_pub.publish(sine)
            rate.sleep()

def main():
    rospy.init_node( 'sine' )
    sn = SineNode()
    sn.run()

if __name__ == '__main__':
    main()
    
